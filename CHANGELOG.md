## [0.2.4](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/compare/v0.2.3...v0.2.4) (2024-02-23)


### CI/CD

* Upgrade semantic-release nodejs version to 20 ([7cb5ac2](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/7cb5ac25048e410cdd10da6f4dba2cd30ab7ac81))

## [0.2.3](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/compare/v0.2.2...v0.2.3) (2023-06-09)


### CI/CD

* Update CI template to v0.15.0 ([8b2e227](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/8b2e227d0992591799bb20fdd4efbe5735cc9d09))

## [0.2.2](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/compare/v0.2.1...v0.2.2) (2023-06-08)


### CI/CD

* Update general-template to v0.14.1 ([693fe8c](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/693fe8c1c5afa8f2c653f590808c12d7c4934382))
* Update node to 18 in semantic-release ([7600088](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/7600088472e46427613a9cd6ca1093ae61ad3691))

### [0.2.1](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/compare/v0.2.0...v0.2.1) (2022-01-27)


### CI/CD

* Add editorconfig-lint ([b2cd6ab](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/b2cd6ab8caa5a2f442edd3860d3990cd761f9404))
* Add jsonlint ([9cac90c](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/9cac90c352dea6284807f73b7c50056b072975d0))
* Add semantic-release ([3b9ce03](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/3b9ce035390d82239fc9f7c5f0a5299a48b74d25))
* Update yamllint template version ([9cf2273](https://gitlab.com/kklab-public/gitlab-ci-templates/puppet-template/commit/9cf22739b8ac433e087ebeca9cf2f7b604abb667))

# Change log

## v0.1.0

Features:

- Add [.deploy](deploy/deploy.yml)
- Add [.puppet-lint](test/puppet-lint.yml)
- Add [.puppet-validate](test/puppet-validate.yml)
- Add [.uid-conflicts-detect](test/uid-conflicts-detect.yml)

## v0.2.0

Features:

- Add [.account-alphabetical-order-detect.yml](test/account-alphabetical-order-detect.yml)

Updates:

- Ignore .git folder in [.puppet-validate](test/puppet-validate.yml)
